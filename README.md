# Gollum #

![alternativetext](doc/gollum.png)

* [Description](#description)
* [Importing package](#importing)
* [Documentation](#documentation)
* [FAQ](#faq)
* [License](#license)

### Description ###

Gollum is an enhanced fasthttp client library written in Go with minimal zero allocation. :

### Importing

Before importing gollum into your code, first download gollum using `go get` tool.

```bash
$ go get bitbucket.org/rctiplus/gollum 
```

To use the gollum, simply adding the following import statement to your `.go` files.

```go
import "bitbucket.org/rctiplus/gollum"
```

## Documentation

Further documentation can be found on [pkg.go.dev](https://pkg.go.dev/bitbucket.org/rctiplus/gollum)

## FAQ

**Can I contribute to make Almasbub?**

[Please do!](https://bitbucket.org/rctiplus/gollum/blob/master/CONTRIBUTING.md) We are looking for any kind of contribution to improve gollum core funtionality and documentation. When in doubt, make a PR!


## Benchmark test

TODO : add benchmark test

## License

```
Copyright (c) 2021, RCTIPlus

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```